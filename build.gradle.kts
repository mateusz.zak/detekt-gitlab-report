plugins {
    kotlin("jvm") version "1.5.30" apply false
    kotlin("plugin.serialization") version "1.5.30" apply false
    id("io.gitlab.arturbosch.detekt") version "1.18.1" apply false
    id("com.github.johnrengelman.shadow") version "7.0.0" apply false
}
