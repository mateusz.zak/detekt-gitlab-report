package com.gitlab.cromefire.detektgitlab.models

import kotlinx.serialization.Serializable

@Serializable
internal data class Location(
    /**
     * The relative path to the file containing the code quality violation.
     */
    val path: String,
    /**
     * The position at which the code quality violation occurred.
     */
    val positions: Positions
)
