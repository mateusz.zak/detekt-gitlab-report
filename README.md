# Detekt GitLab (/ Code Climate) Report

[![maven version](https://img.shields.io/maven-metadata/v?metadataUrl=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F25796063%2Fpackages%2Fmaven%2Fcom%2Fgitlab%2Fcromefire%2Fdetekt-gitlab-report%2Fmaven-metadata.xml)](https://gitlab.com/cromefire_/detekt-gitlab-report/-/packages)

Reports the detekt violations in a format that
GitLab's [Code Quality Feature](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html) understands.

This is designed for use with GitLab, but should also work fine with everything else that accepts the code climate format.

## Installation
### Gradle

```kotlin
repositories {
    maven(url = "https://gitlab.com/api/v4/projects/25796063/packages/maven")
}

detekt {
    // [...]

    // Has to be specified for it to correctly report the paths to GitLab
    basePath = rootDir.toString()

    reports {
        custom {
            reportId = "DetektGitlabReport"
            // This tells detekt, where it should write the report to,
            // you have to specify this file in the gitlab pipeline config.
            destination = file("$buildDir/reports/detekt/gitlab.json")
        }
    }
}

dependencies {
    detektPlugins("com.gitlab.cromefire:detekt-gitlab-report:0.2.1")
}
```

### Other tools
See the [detekt documentation](https://detekt.github.io) on how to add a plugin, you can find a jar in the artifacts linked to [a release tag](https://gitlab.com/cromefire_/detekt-gitlab-report/-/tags?search=v&sort=name_desc).

## Configure pipeline

You just have to add the artifact to the pipeline in order to get it working:

```yaml
test:
  # [...]
  script: "./gradlew check"
  artifacts:
    reports:
      codequality: "build/reports/detekt/gitlab.json"
```

## Troubleshooting

### Relative path not available.

You probably forgot to configure the `basePath`.
